package fr.ulille.iut.pizzaland.beans;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class Pizza {
	private UUID id = UUID.randomUUID();
	private String name;
	private List<Ingredient> list=new ArrayList<>();
	
	public Pizza() {
    }

    public Pizza(String name) {
        this.name = name;
    }

    public Pizza(UUID id, String name) {
        this.id = id;
        this.name = name;
    }

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	public List<Ingredient> getList() {
		return list;
	}

	public void setList(List<Ingredient> list) {
		this.list = list;
	}

	public static PizzaDto toDto(Pizza i) {
        PizzaDto dto = new PizzaDto();
        dto.setId(i.getId());
        dto.setName(i.getName());

        return dto;
    }
    
    public static Pizza fromDto(PizzaDto dto) {
    	Pizza pizza = new Pizza();
    	pizza.setId(dto.getId());
    	pizza.setName(dto.getName());

        return pizza;
    }

	@Override
	public String toString() {
		return "Pizza [id=" + id + ", name=" + name + ", list=" + list + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((list == null) ? 0 : list.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pizza other = (Pizza) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (list == null) {
			if (other.list != null)
				return false;
		} else if (!list.equals(other.list))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
    
	public static PizzaCreateDto toCreateDto(Pizza pizza) {
	    PizzaCreateDto dto = new PizzaCreateDto();
	    dto.setName(pizza.getName());
	    List<UUID> li=new ArrayList<>();
	    for(Ingredient in : pizza.getList()) {
	    	li.add(in.getId());
	    }
	    dto.setList(li);
	    return dto;
	  }
		
	  public static Pizza fromPizzaCreateDto(PizzaCreateDto dto) {
	    Pizza pizza = new Pizza();
	    pizza.setName(dto.getName());
	    List<Ingredient> li=new ArrayList<>();
	    IngredientDao ing = BDDFactory.buildDao(IngredientDao.class);
	    for(UUID uu : dto.getList()) {
	    	li.add(ing.findById(uu)); 
	    }
	    
	    pizza.setList(li);
	    return pizza;
	  }

}