package fr.ulille.iut.pizzaland.dao;

import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;

public interface PizzaDao {
	
  @SqlUpdate("CREATE TABLE IF NOT EXISTS Pizzas (id VARCHAR(128) PRIMARY KEY, name VARCHAR UNIQUE NOT NULL)")
  void createPizzaTable();
  
  @SqlUpdate("DROP TABLE IF EXISTS Pizzas")
  void dropPizzaTable();
  
  @SqlUpdate("INSERT INTO Pizzas (id, name) VALUES (:id, :name)")
  void insert(@BindBean Pizza pizza);
  
  @SqlQuery("SELECT * FROM Pizzas WHERE name = :name")
  @RegisterBeanMapper(Pizza.class)
  Pizza findByName(@Bind("name") String name);
  
  @SqlQuery("SELECT * FROM Pizzas")
  @RegisterBeanMapper(Pizza.class)
  List<Pizza> getAll();
  
  @SqlQuery("SELECT * FROM Pizzas WHERE id = :id")
  @RegisterBeanMapper(Pizza.class)
  Pizza findById(@Bind("id") UUID id);

  @SqlUpdate("DELETE FROM Pizzas WHERE id = :id")
  void remove(@Bind("id") UUID id);
  
  @SqlUpdate("CREATE TABLE IF NOT EXISTS PizzaIngredientsAssociation (idPizza VARCHAR(128) NOT NULL, idIngredient VARCHAR(128) NOT NULL)")
  void createAssociationTable();
  
  @SqlUpdate("INSERT INTO PizzaIngredientsAssociation (idPizza, idIngredient) VALUES (:idPizza, :idIngredient)")
  void insertAssociation(@Bind("idPizza") UUID idPizza, @Bind("idIngredient") UUID idIngredient);
  
  @SqlQuery("SELECT * FROM ingredients, PizzaIngredientsAssociation WHERE id = idIngredient AND idPizza = :idPizza2")
  @RegisterBeanMapper(Ingredient.class)
  List<Ingredient> getListeIngredient(@Bind("idPizza2")UUID idPizza);
  
  @SqlUpdate("DROP TABLE IF EXISTS PizzaIngredientsAssociation")
  void dropAssociationTable();
  
  @Transaction
  default void insertPizzaWithIngredient(Pizza pizza) {
	  insert(pizza);
	  UUID pizzaId=pizza.getId();
	  for(Ingredient i : pizza.getList()) {
		  insertAssociation(pizzaId, i.getId());
	  }
  }


@Transaction
  default void createTableAndIngredientAssociation() {
    createAssociationTable();
    createPizzaTable();
  }

@Transaction
  default Pizza findByIdWithIngredient(UUID id) {
		Pizza pizza= findById(id);
		pizza.setList(getListeIngredient(id));
		return pizza;
	}
}