<table data-sourcepos="60:1-66:194" dir="auto">
<thead>
<tr data-sourcepos="60:1-60:197">
<th align="left" data-sourcepos="60:2-60:27">URI</th>
<th align="left" data-sourcepos="60:29-60:42">Opération</th>
<th align="left" data-sourcepos="60:44-60:105">MIME</th>
<th align="left" data-sourcepos="60:107-60:124">Requête</th>
<th align="left" data-sourcepos="60:126-60:196">Réponse</th>
</tr>
</thead>
<tbody>
<tr data-sourcepos="62:1-62:195">
<td align="left" data-sourcepos="62:2-62:27">/pizzas</td>
<td align="left" data-sourcepos="62:29-62:41">GET</td>
<td align="left" data-sourcepos="62:43-62:104">&lt;-application/json<br>&lt;-application/xml</td>
<td align="left" data-sourcepos="62:106-62:122"></td>
<td align="left" data-sourcepos="62:124-62:194">liste des pizzas (P2)</td>
</tr>
<tr data-sourcepos="63:1-63:195">
<td align="left" data-sourcepos="63:2-63:27">/pizzas/{id}</td>
<td align="left" data-sourcepos="63:29-63:41">GET</td>
<td align="left" data-sourcepos="63:43-63:104">&lt;-application/json<br>&lt;-application/xml</td>
<td align="left" data-sourcepos="63:106-63:122"></td>
<td align="left" data-sourcepos="63:124-63:194">une pizza (P2) ou 404</td>
</tr>
<tr data-sourcepos="64:1-64:195">
<td align="left" data-sourcepos="64:2-64:27">/pizzas/{id}/name</td>
<td align="left" data-sourcepos="64:29-64:41">GET</td>
<td align="left" data-sourcepos="64:43-64:104">&lt;-text/plain</td>
<td align="left" data-sourcepos="64:106-64:122"></td>
<td align="left" data-sourcepos="64:124-64:194">le nom de la pizza ou 404</td>
</tr>
<tr data-sourcepos="65:1-65:200">
<td align="left" data-sourcepos="65:2-65:27">/pizzas</td>
<td align="left" data-sourcepos="65:29-65:41">POST</td>
<td align="left" data-sourcepos="65:43-65:104">&lt;-/-&gt;application/json<br>-&gt;application/x-www-form-urlencoded</td>
<td align="left" data-sourcepos="65:106-65:123">Pizza (P1)</td>
<td align="left" data-sourcepos="65:125-65:199">Nouvelle pizza (P2)<br>409 si la pizza existe déjà (même nom)</td>
</tr>
<tr data-sourcepos="66:1-66:194">
<td align="left" data-sourcepos="66:2-66:27">/pizzas/{id}</td>
<td align="left" data-sourcepos="66:29-66:41">DELETE</td>
<td align="left" data-sourcepos="66:43-66:104"></td>
<td align="left" data-sourcepos="66:106-66:122"></td>
<td align="left" data-sourcepos="66:124-66:193"></td>
</tr>
</tbody>
</table>

Une Pizza comporte un identifiant, un nom et une liste d'Ingrédients.